# Welcome to Guac a Mole

Smash as many avocados as possible before the timer is up! Make some delicious guacamole.

## Install and play

Fork and clone the git clone.
Cd into the **whac-a-mole-game** folder.
Make sure Docker Desktop is running.
Enter these commands in terminal:
`docker-compose build`
`docker-compose up`
Go to localhost:3000 in your browser once React has compiled.
Have fun!
