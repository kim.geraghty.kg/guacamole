import React from "react";
import { useEffect, useRef, useState } from "react";
import { gsap } from 'gsap';


function Mole({points, onWhack, delay}) {

  // we use Refs instead of classes to tell gsap which elements to target. React recommends against using classes or selector text.
  // refs: an attribute which makes it possible to store a reference to particular DOM nodes or React elements

  const avocadoCoolRef = useRef(null)
  const avocadoCuteRef = useRef(null)
  const avocadoRunRef = useRef(null)
  const avocadoHatRef = useRef(null)
  const avocadoSunRef = useRef(null)

  const coolRef = useRef(null)
  const cuteRef = useRef(null)
  const runRef = useRef(null)
  const hatRef = useRef(null)
  const sunRef = useRef(null)

    // whacked function attempt
    const [whacked, setWhacked] = useState(false)
    const [cuteWhacked, setCuteWhacked] = useState(false)
    const [runWhacked, setRunWhacked] = useState(false)
    const [hatWhacked, setHatWhacked] = useState(false)
    const [sunWhacked, setSunWhacked] = useState(false)

    const coolWhack = () => {
      onWhack(points);
      setWhacked(true);
      console.log("cool")
    }

    const cuteWhack = () => {
      onWhack(points);
      setCuteWhacked(true);
      console.log("cute")
    }

    const runWhack = () => {
      onWhack(points);
      setRunWhacked(true);
      console.log("run")
    }

    const hatWhack = () => {
      onWhack(points);
      setHatWhacked(true);
      console.log("hat")
    }

    const sunWhack = () => {
      onWhack(points);
      setSunWhacked(true);
      console.log("sun")
    }

  // cool avocado animation
  useEffect(() => {
      gsap.set(avocadoCoolRef.current, { y: 500}) // accelerate this percentage over time to increase difficulty
      coolRef.current = gsap.to(avocadoCoolRef.current, {
        y: 0,
        duration: gsap.utils.random(1,3),
        // ease: "power4.out",
        yoyo: true,
        repeat: -1,
      })
  }, [])

  // rerender when cool whacked
  useEffect(() => {
    if (whacked) {
      coolRef.current.pause()
      gsap.to(avocadoCoolRef.current, {
        y: 0,
        yoyo: false,
        duration: 0.1,
        onComplete: () => {
            setWhacked(false);
            coolRef.current
             .restart()
             .timeScale(coolRef.current.timeScale() * 1.1)
        }})}
  }, [whacked])

  // cute avocado animation:
  useEffect(() => {
    gsap.set(avocadoCuteRef.current, { y: 500})
    cuteRef.current = gsap.to(avocadoCuteRef.current, {
      y: 0, // send to bottom
      yoyo: true,
      duration: gsap.utils.random(1,3),
      repeat: -1,
      delay: gsap.utils.random(0.25,1),
    })
  }, [])

  // rerender when cute whacked
  useEffect(() => {
    if (cuteWhacked) {
      cuteRef.current.pause()
      gsap.to(avocadoCuteRef.current, {
        y: 0,
        yoyo: false,
        duration: 0.1,
        onComplete: () => {
            setCuteWhacked(false);
            cuteRef.current
             .restart()
             .timeScale(cuteRef.current.timeScale())
        }})}
  }, [cuteWhacked])

  // run avocado animation:
  useEffect(() => {
    gsap.set(avocadoRunRef.current, { y: 200, xPercent: -100})
    runRef.current = gsap.to(avocadoRunRef.current, {
      y: 0, // send to bottom
      xPercent: 0,
      duration: gsap.utils.random(1,3),
      yoyo: true,
      repeat: -1,
      scale: 1.2,
    })
  }, [])

  // rerender when run whacked
  useEffect(() => {
    if (runWhacked) {
      runRef.current.pause()
      gsap.to(avocadoRunRef.current, {
        y: 0,
        yoyo: false,
        duration: 0.1,
        onComplete: () => {
            setRunWhacked(false);
            runRef.current
             .restart()
             .timeScale(runRef.current.timeScale())
        }})}
  }, [runWhacked])

  // hat avocado animation:
  useEffect(() => {
    gsap.set(avocadoHatRef.current, { y: 100})
    hatRef.current = gsap.to(avocadoHatRef.current, {
      y: 0, // send to bottom
      duration: gsap.utils.random(1,3),
      yoyo: true,
      repeat: -1,
      delay: gsap.utils.random(0.25,1),
    })
  }, [])

  // rerender when hat whacked
  useEffect(() => {
    if (hatWhacked) {
      hatRef.current.pause()
      gsap.to(avocadoHatRef.current, {
        y: 0,
        yoyo: false,
        duration: 0.1,
        onComplete: () => {
            setHatWhacked(false);
            hatRef.current
             .restart()
             .timeScale(runRef.current.timeScale())
        }})}
  }, [hatWhacked])

    // sun avocado animation:
    useEffect(() => {
      gsap.set(avocadoSunRef.current, { y: 100})
      sunRef.current = gsap.to(avocadoSunRef.current, {
        y: 0, // send to bottom
        duration: gsap.utils.random(1,3),
        ease: "power4.out",
        yoyo: true,
        repeat: -1,
        delay: gsap.utils.random(0.75,1.25),
      })
    }, [])

  // rerender when sun whacked
  useEffect(() => {
    if (sunWhacked) {
      sunRef.current.pause()
      gsap.to(avocadoSunRef.current, {
        y: 0,
        yoyo: false,
        duration: 0.1,
        onComplete: () => {
            setSunWhacked(false);
            sunRef.current
             .restart()
             .timeScale(runRef.current.timeScale())
        }})}
  }, [sunWhacked])

// two approaches:
// 1. set function for onClick for button (ex. cool and cute)
// 2. separate two onClick effects between button and image (ex. run)

  return (
    <div className="guac">
      <div className="gsap">
      <button ref={avocadoCoolRef} onClick={coolWhack} className="mole">
        <img className="avocado" src="images/avocado-cool.png" alt="cool avocado" />
        </button>
      <button ref={avocadoCuteRef} onClick={cuteWhack} className="mole">
      <img className="avocado" src="images/avocado-cutie.png" alt="cutie avocado" />
        </button>
      <button ref={avocadoRunRef} onClick={() => onWhack(points)} className="mole">
      <img onClick={runWhack} className="avocado" src="images/avocado-running.png" alt="running avocado" />
      </button>
      <button ref={avocadoHatRef} onClick={() => onWhack(points)} className="mole">
      <img onClick={hatWhack} className="avocado" src="images/avocado-with-hat.png" alt="hat avocado" />
      </button>
      <button ref={avocadoSunRef} onClick={() => onWhack(points)} className="mole">
        <img onClick={sunWhack} className="avocado" src="images/avocado-sunbathing.png" alt="sunny avocado" />
        </button>
        </div>
    </div>
  )

}

export default Mole;
