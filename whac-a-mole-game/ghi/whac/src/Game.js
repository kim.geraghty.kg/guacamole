import React, { Fragment } from "react";
import { useState } from "react";
import Timer from "./Timer";
import Mole from "./Mole";
import { gsap } from 'gsap';

function Game() {

  // starts on false, changes to true when we click button
  const [playing, setPlaying] = useState(false)

  const [end, setEnd] = useState(false)

  // creating our score with state, starting at 0
  const [score, setScore] = useState(0)

  const points = 100;

  const [delay, setDelay] = useState(0)

  // creating our onWhack function, updating score with setScore from useState
  const onWhack = points => setScore(score + points);

  // creating start and finish functions to change the state of the game:
  const startGame = () => {
    setScore(0);
    setPlaying(!playing);
    setEnd(false)
    setDelay(gsap.utils.random(0.5, 4))
  }

  const endGame = () => {
    setPlaying(false);
    setEnd(true);
  }

  // console.log("playing?", playing);
  // console.log("finished?", end)

  return (
    <Fragment>

    {/* first state: when not playing and never played, we start here */}
    {!playing && !end}
        <button className="start-stop container" onClick={ () => startGame()}>
        {playing ? "Stop" : "Start"}
        {/* ternary expression */}
        </button>


      {/* second state: if playing is true, it activates the game */}
      {playing && !end &&
        <Fragment>
            <button className="end-game container" onClick={endGame}>End Game</button>
            <Timer endGame={endGame} />
            <div className="score container">Score: {score}</div>
          <Mole points={points} onWhack={onWhack} delay={delay}/>
        </Fragment>
      }

        {/* third state: after playing and ending a game */}
        {end && !playing &&
        <Fragment>
          <button className="start-game container" onClick={startGame}>Play again</button>
          <div className="timer container">Game over! </div>
          <div className="score container">Score: {score}</div>
        </Fragment>
      }

    </Fragment>
  )

}

export default Game;
