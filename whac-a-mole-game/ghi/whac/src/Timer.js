import React from "react";
import { useEffect, useState } from "react";


// endGame passed in as a function, needs brackets
function Timer({endGame}) {

// creating gameTime variable and giving it state and a starting point
  const [gameTime, setGameTime] = useState(30000); // 20000 is equivalent to 20 seconds

const interval = 1000;

// starting the Effect
useEffect(() => {
  // it ends when gameTime reaches 0
      if (!gameTime) return;

      // useEffect starts the countdown function at intervals of 1 second (1000)
      const timer = setInterval(() => setGameTime(gameTime - 1000), interval);
      // console.log("running", timer);

      // we clear the timer (aka setInterval) at the end of each useEffect loop
      // to avoid memory leaks on re-render
      return () => {
        clearInterval(timer);
      }
    },
    // needed to add this dependancy to determine when the rerender occurs
    // so it is defined by the interval, and not interrupted by other rerenders
    // without this, the onClick from the Mole would temporarily interrupt the timer
    [gameTime, interval]);

    // starting the Effect for endGame
    useEffect(() => {
      if (!gameTime && endGame) endGame();
    })


// this countdown function is now directly in the setInterval above
  // function countdown() {
  //   const time = gameTime - 1000;
  //   setGameTime(time)
  // }

// // starting the Effect
//   useEffect(() => {
// // it ends when gameTime reaches 0
//     if (!gameTime) return; }



  return (
  <div className="timer container">
    {`Time: ${gameTime/1000}`}
  </div>
  )
}


export default Timer;
